# Next.JS Project

Just run `npm install` to get started. You may want to update the project name in package.json.

## Getting Started

You need to add the `DOKKU_KEY` var in the Gitlab project to be able to deploy the project automatically for each branch. Ask around for the Dokku Key SSH Key, it's the same in most projects.

## Linter

There's a precommit hook to run Prettier automatically on all staged files. In case you need more commands, you can run:

- `npm run lint:fix`: Fixes ESLint issues
- `npm run prettier`: Applies prettier on the entire repo.
  s
