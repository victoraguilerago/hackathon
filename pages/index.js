import React, { Fragment } from 'react';

export default () => (
  <Fragment>
    <img src="/static/aerolab-brand.svg" alt="aerolab logo" className="aerolab-logo" />
    <style jsx>{`
      .aerolab-logo {
        width: 25vw;
      }
    `}</style>
  </Fragment>
);
